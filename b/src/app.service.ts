import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { create } from 'domain';
import { Repository } from 'typeorm';
import { moonoy } from './wiraphat.entity';


@Injectable()
export class AppService {
  constructor(
    @InjectRepository(moonoy)private readonly mooRepository:Repository<moonoy>

  ){}
  
    async create(data : any): Promise<moonoy>{
      console.log(data);
      return this.mooRepository.save(data);
      
    }
  
}

