import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {
 
  @Input() idcardgreen: string ="";

  @Input() idnamegreen: string ="Wiraphat";

  constructor() { }

  ngOnInit(): void {
  }

}
