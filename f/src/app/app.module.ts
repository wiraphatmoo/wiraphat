import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoopPipe } from './loop.pipe';
import { FormsModule } from '@angular/forms';
import { CardComponent } from './card/card.component';
import { HttpClientModule } from '@angular/common/http';
import { AppService } from './app.service';



@NgModule({
  declarations: [
    AppComponent,
    LoopPipe,
    CardComponent
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    

  ],
  providers: [AppService],
  bootstrap: [AppComponent]
  
})
export class AppModule { }
