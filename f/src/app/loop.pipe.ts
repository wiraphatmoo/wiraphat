import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'loop'
})
export class LoopPipe implements PipeTransform {

  transform(value: any, args?: any): any {

    let card: string = value;
    
    for (let i = 0; i <=16; i++) {
      card += '#';
    }
 
    return card.substr(0,4) + " " + card.substr(4,4) + " " + card.substr(8,4) + " " + card.substr(12,4); 
  }

}
